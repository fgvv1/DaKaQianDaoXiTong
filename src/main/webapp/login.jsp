<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登录页</title>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/base_style.css"/>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/login.css"/>
    <script type="text/javascript" src="/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="/static/skin/js/login.js"></script>
    <style type="text/css">

    </style>
</head>

<body>
<div class="login_titbg1">
    <div class="login_titbg2">
        <div class="content_layout">
            <h1 class="fyh tit_text">赣州某某工作管理系统</h1>
            <em class="tit_em">GANZHOU&nbsp;MOMO&nbsp;WORK&nbsp;MANAGERIAL&nbsp;SYSTEM&nbsp;&nbsp;V1.0</em>
            <div class="fyh time_layout">
                <div class="welcome">欢迎您！&nbsp;请登录赣州某工作管理系统</div>
                <div class="time">今天是：<span id="time"></span></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<div class="form_layout">
    <form action="login" method="post" class="form_lay">
        <div class="form_tit">
            <img src="/static/skin/images/form_icon_03.png" class="dq"/>
            <span class="form_tittext fyh">用户登录&nbsp;&nbsp;LOGIN</span>
        </div>
        <div class="hr-20"></div>
        <div class="text_layout">
            <label class="text_lab fyh right_4" for="username">用户名：</label>
            <input type="text" class="form_text fyh" id="username" name="username"/>
            <div class="ts_text fyh">输入您的用户名</div>
        </div>
        <div class="text_layout">
            <label class="text_lab fyh right_6" for="pw">密&nbsp;&nbsp;&nbsp;码：</label>
            <input type="password" class="form_text fyh" name="password" id="password"/>
            <div class="ts_text fyh">输入您的密码</div>
        </div>
        <div class="text_layout">
            <label class="text_lab fyh right_4" for="yzm">验证码：</label>
            <input type="text" class="form_text1 fyh" id="yzm" name="yzm"/>
            <span class="show fyh">SFRW</span>
            <em class="change_yzm fyh">看不清，换一个</em>
        </div>
        <div class="hr-30"></div>
        <input type="submit" value="登&nbsp;&nbsp;&nbsp;录" class="button fyh"/>
    </form>
    <div class="clear"></div>
</div>
<div class="floor fyh">软件开发：&nbsp;赣州科睿特软件技术有限公司(技术支持)&nbsp;&nbsp;&nbsp;&nbsp;客服电话：&nbsp;0797-8356742</div>
</body>
</html>
