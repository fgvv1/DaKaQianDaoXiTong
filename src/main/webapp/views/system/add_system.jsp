<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/form.js"></script>
    <script>
        $(function () {
            $("#roleId").change(function(){
//                var sMenuId =$("#sMenuId");
//                $("#sMenuId").each(function (i,n) {
//                    if(n.text=="请选择"){
//
//                    }else {
//                        n.html="";
//                    }
//                })
                $("#sMenuId").html("")
            var roleId = $("#roleId").val();
            var url = "${pageContext.request.contextPath}/add1_system";
            var param = {"roleId":roleId}
            $.post(url,param,function (data) {
                $(data).each(function (i,n) {

                    $("#sMenuId").append("<option value='"+n.smenuId+"'>"+n.sname+"</option>");
                })
            },"json")
        })

        })
        $(function () {
            $("#checkButton").click(function () {
                var pwd =$("#checkPwd").val();
                if(pwd=="${userInfo.password}"){
                    $("#submit").submit();
                }else {
                    alert("密码错误");
                }
            })
        })
    </script>
</head>
<body>
<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>分配权限</b></span></hr>
</div>
<form action="${pageContext.request.contextPath}/addPermission_system" method="post" id="submit">
    <table class="fm_tab">
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">选择角色：</span></td>
            <td class="fm_td2">
                <select name="roleId" id="roleId" >
                    <option value="" selected  >请选择</option>
                    <option value="0">普通员工</option>
                    <option value="1">部长</option>
                    <option value="2">总经理</option>
                    <option value="3">管理员</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">权限：</span></td>
            <td width="35%" class="fm_td2">
                <select name="sMenuId" id="sMenuId">

                </select>
            </td>
        </tr>
    </table>
        <div style="padding-top: 20px;padding-left: 300px;">
            <input style="width: 120px;"  type="button" id="submit_id" class="fm_button fyh submit_id" value="确&nbsp;&nbsp;定"/>
        </div>

</form>
<div class="form_zz">
    <div class="form_zzbg"></div>
    <div class="form_zzcontent">
        <h1 class="fyh form_zztit"><img src="/static/skin/images/form_zz_icon_03.png" class="form_zzimg"/>提示信息</h1>
        <div class="form_ts fyh">
            请输入密码:<input id="checkPwd" type="text" placeholder="" >
        </div>
        <div class="form_zzbutton">
            <input id="checkButton" type="button" value="确&nbsp;定" class="form_zzan1 fyh"/>
            <input type="button" value="取&nbsp;消" class="form_zzan2 fyh"/>
        </div>
    </div>
</div>
</body>
</html>
