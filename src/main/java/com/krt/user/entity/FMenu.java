package com.krt.user.entity;

public class FMenu {
    private Integer fMenuId;//一菜单id

    private String fName;//一菜单名称

    public Integer getfMenuId() {
        return fMenuId;
    }

    public void setfMenuId(Integer fMenuId) {
        this.fMenuId = fMenuId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName == null ? null : fName.trim();
    }
}