package com.krt.common;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * Created by 陈汉帅 on 2017-12-26.
 */
public class CustomCredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        try {
            UsernamePasswordToken userToken = (UsernamePasswordToken) token;
            String password= String.valueOf(userToken.getPassword());
            Object DBPassword= getCredentials(info);
            return this.equals(password,DBPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
