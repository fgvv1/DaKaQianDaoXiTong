<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script >
        function checkPwd() {
            var pwd1= $("#new_pwd").val();
            var pwd=$("#agin_new_pwd").val();
            if($("#old_pwd").val().length==0){
                alert("旧密码不能为空")
                return false;
            }
            if($("#old_pwd").val()!=${userInfo.password}){
                alert("旧密码错误")
                return false;
            }
            if(pwd1.length==0){
                alert("新密码不能为空");
                return false;
            }
            if (pwd==(pwd1)){

            }else {
                alert("两次输入的密码不一致");
                return false;
            }
        }
    </script>
</head>
<body>
<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>修改密码</b></span></hr>
</div>
<form action="${pageContext.request.contextPath}/updatePwd_user" method="post" onsubmit="return checkPwd()">
    <table class="fm_tab">
            <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">密码验证：</span></td>
            <td width="35%" class="fm_td2"><input type="text" id="old_pwd"   name="old_pwd"  class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
        <td width="15%" class="fm_td1 fyh"><span class="fm_text">新密码：</span></td>
        <td width="35%" class="fm_td2"><input type="password" id="new_pwd"   name="password"   class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
        <td width="15%" class="fm_td1 fyh"><span class="fm_text">再次输入新密码：</span></td>
        <td width="35%" class="fm_td2"><input type="password" onblur="checkPwd()" id="agin_new_pwd"   name="agin_new_pwd"   class="fm_tt1" style="width:25%"/></td>
        </tr>
    </table>
            <div style="padding-top: 20px;padding-left: 300px;">
                <input style="width: 80px;"  type="submit" id="submit_id" class="table_bt fyh submit_id" value="更&nbsp;&nbsp;新"/>
            </div>
        </form>
</body>
</html>
