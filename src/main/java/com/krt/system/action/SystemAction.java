package com.krt.system.action;

import com.alibaba.fastjson.JSON;
import com.krt.common.Page;
import com.krt.system.service.RoleService;
import com.krt.user.entity.Role;
import com.krt.user.entity.RolePermission;
import com.krt.user.entity.SMenu;
import com.krt.user.service.RolePermissionService;
import com.krt.user.service.SMenuService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
public class SystemAction extends ActionSupport implements ModelDriven<RolePermission>{
    /**
    *@Description:管理系统模块及功能
    *@Data:13:49 2018-01-07
    */
    @Autowired
    private RoleService roleService;
    @Autowired
    private SMenuService sMenuService;
    @Autowired
    private RolePermissionService rolePermissionService;
    //封装第二菜单的对象
    private SMenu sMenu;
    //页数
    private Integer CurrentPage = 1;

    public Integer getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        CurrentPage = currentPage;
    }

    private RolePermission rolePermission = new RolePermission();

    public SMenu getsMenu() {
        return sMenu;
    }

    public void setsMenu(SMenu sMenu) {
        this.sMenu = sMenu;
    }


    /**
    *@Description:回收权限
    *@Data:21:36 2018-01-07
    */
    public String manage(){
        //获取所有的角色权限关系
        Page page= roleService.getAllRolePermission(CurrentPage);
        ValueStack vs = ActionContext.getContext().getValueStack();
        vs.set("page",page);
        return "manage";
    }
    /**
    *@Description:删除权限
    *@Data:17:26 2018-01-08
    */
    public String delete(){
        int result = rolePermissionService.deleteRP(rolePermission);
        if (result>0){
            return "delete";
        }
        return "faile";
    }

    public String add(){

        return "add";
    }
    /**
    *@Description:添加权限
    *@Data:17:26 2018-01-08
    */
    public String addPermission(){
        int result = rolePermissionService.addPermission(rolePermission);
        return "addPermission";
    }
    /**
    *@Description:ajax根据角色查询没有的权限
    *@Data:17:26 2018-01-08
    */
    public void add1() throws IOException {
        //未赋予的权限
        int roleId = Integer.parseInt(ServletActionContext.getRequest().getParameter("roleId"));
        List<SMenu> smenuList = sMenuService.selectSMenu(roleId);
        //查询出未赋予权限的list
        String s= JSON.toJSONString(smenuList);
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().print(s);
    }


    @Override
    public RolePermission getModel() {
        return rolePermission;
    }
}
