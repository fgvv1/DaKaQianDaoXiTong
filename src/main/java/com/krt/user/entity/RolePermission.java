package com.krt.user.entity;

import java.util.List;

public class RolePermission {
    private Integer roleId;//角色id

    private Integer sMenuId;//二菜单id

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getsMenuId() {
        return sMenuId;
    }

    public void setsMenuId(Integer sMenuId) {
        this.sMenuId = sMenuId;
    }
}