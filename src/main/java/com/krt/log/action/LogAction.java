package com.krt.log.action;

import com.alibaba.fastjson.JSON;
import com.krt.common.Page;
import com.krt.log.entity.Log;
import com.krt.log.entity.LogClazz;
import com.krt.log.service.LogClazzService;
import com.krt.log.service.LogService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**日志的增删改查
 * Created by 陈汉帅 on 2017-12-26.
 */
public class LogAction extends ActionSupport implements ModelDriven<Log>{
    @Autowired
    private LogService logService;
    @Autowired
    private LogClazzService logClazzService;

    private String logClazzname;//日志类型名

    public String getLogClazzname() {
        return logClazzname;
    }

    public void setLogClazzname(String logClazzname) {
        this.logClazzname = logClazzname;
    }
    //日志模型驱动
    private Log log=new Log();
    //当前页数
    private Integer CurrentPage = 1;
    public void setCurrentPage(int currentPage) {
        CurrentPage = currentPage;
    }
    private Integer TotalPage=1;
    public void setTotalPage(Integer totalPage) {
        TotalPage = totalPage;
    }
    //查询信息
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    /**
    *@Description:写日志
    *@Data:21:31 2018-01-01
    */
    @RequiresPermissions("add_log")
    public String add() throws IOException {
        //先把日志类型查出来
        List<LogClazz> logClazzList= logService.getLogClazz();
        ServletActionContext.getRequest().setAttribute("logClazzList",logClazzList);
        return "add";
    }
    /**
    *@Description:保存日志
    *@Data:12:14 2018-01-02
    */
    public String save() throws ParseException {
        int result = logService.saveLog(log);
        if (result>0){
            return "save";
        }
        return "faile";
    }
    /**
    *@Description:管理日志
    *@Data:13:55 2018-01-02
    */
    public String manage() throws IOException {
        //查出类型
        List<LogClazz> logClazzList= logService.getLogClazz();
        //将查出的log封装到page里
        Page page= logService.manageLog(log.getLogClazz(),log.getLogStatus(),info,CurrentPage);
        ValueStack valueStack = ActionContext.getContext().getValueStack();
        //将数据封装进值栈
        valueStack.set("page",page);
        valueStack.set("logClazzList",logClazzList);
        return "manage";
    }

    /**
    *@Description:查看日志
    *@Data:17:35 2018-01-03
    */
    public String look(){
        //按日志的id查看日志
        Log log1 = logService.lookLog(log.getLogId());
        ValueStack vs =ActionContext.getContext().getValueStack();
        vs.set("log1",log1);
        return "look";
    }
    /**
    *@Description:员工修改日志
    *@Data:19:16 2018-01-03
    */
    public String editor(){
        List<LogClazz> logClazzList= logService.getLogClazz();
        Log log1 = logService.lookLog(log.getLogId());
        ValueStack vs =ActionContext.getContext().getValueStack();
        vs.set("log1",log1);
        vs.set("logClazzList",logClazzList);
        return "editor";
    }

    /**
    *@Description:更新日志,成功后跳转管理日志界面
    *@Data:19:48 2018-01-03
    */
    public String update(){
        int result = logService.updateLog(log);
        if (result>0){
            return "update";
        }
        return "faile";
    }
    /**
    *@Description:删除日志
    *@Data:20:26 2018-01-03
    */
    public String delete(){
        int result=logService.deleteLog(log.getLogId());
        if (result>0){
            return "delete";
        }
        return "faile";
    }
    /**
    *@Description:部长审批日志
    *@Data:20:49 2018-01-03
    */
    public String sp(){
        //获取需要审批的日志
        Page page= logService.spLog(CurrentPage);
        ValueStack valueStack = ActionContext.getContext().getValueStack();
        valueStack.set("page",page);
        return "sp";
    }
    /**
    *@Description:部长审批日志成功
    *@Data:9:09 2018-01-04
    */
    public String spSucc(){
        int result = logService.spUpdateLog(log);
        return "spSucc";
    }
    /**
    *@Description:添加日志类型
    *@Data:13:11 2018-01-04
    */
    public String addLogClazz(){
        List<LogClazz> logClazzList= logService.getLogClazz();
        ValueStack vs = ActionContext.getContext().getValueStack();
        vs.set("logClazzList",logClazzList);
        return "addLogClazz";
    }
    /**
    *@Description:日志类型删除
    *@Data:14:48 2018-01-04
    */
    public String deleteLogClazz(){
        int logClazz = Integer.parseInt(ServletActionContext.getRequest().getParameter("logClazz"));
        int result = logClazzService.deleteLogClazz(logClazz);
        if (result>0){
            return "deleteLogClazz";
        }
        return "faile";
    }

    /**
    *@Description:日志类型的添加
    *@Data:15:18 2018-01-04
    */
    public String  addClazzLogSucc(){
        LogClazz logClazz=new LogClazz();
        logClazz.setLogClazzname(logClazzname);
        int result = logClazzService.addClazzLogSucc(logClazz);

        return "addClazzLogSucc";
    }
    /**
    *@Description:ajax将日志类型查出
    *@Data:17:19 2018-01-08
    */
    public void getLogClazz() throws IOException {
        List<LogClazz> logClazzList= logService.getLogClazz();
        HttpServletResponse response=ServletActionContext.getResponse();
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        String s = JSON.toJSONString(logClazzList);
        out.print(s);
    }
    @Override
    public Log getModel() {
        return log;
    }
}
