<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的日志</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script >
        $(function () {
            $("#logStatus").val("${logStatus}")
            $("#text").val("${info}")
            $.ajax({
                url:"/getLogClazz_log",
                dataType:"json",
                type:"post",
                success:function (data) {
                    $(data).each(function (i,n) {
                        var vsId = "${model.logClazz}";
                        if(vsId==n.logClazz){
                            $("#logClazzId").append("<option value='"+n.logClazz+"' selected>"+n.logClazzname+"</option>");
                        }else {
                            $("#logClazzId").append("<option value='"+n.logClazz+"'>"+n.logClazzname+"</option>");
                        }

                    })
                }
            });
        });
    </script>
</head>
<body>
<div class="tabel_choose">
            <form action="${pageContext.request.contextPath}/manage_log"  method="post" >
                <label class="table_lab fyh">类型查询</label>
                <select id="logClazzId" name="logClazz" class="table_select2">
                    <option value="" >请选择</option>
                    <%--<c:forEach items="${logClazzList}" var="logClazz">--%>
                        <%--<option value="${logClazz.logClazz}">${logClazz.logClazzname}</option>--%>
                    <%--</c:forEach>--%>
                </select>
                <c:if test="${userInfo.roleId != 1}">
                <label class="table_lab fyh">状态查询</label>
                <select id="logStatus" name="logStatus" class="table_select2">
                    <option value="" class="">请选择</option>
                    <option value="0">正在上报</option>
                    <option value="1">审核通过</option>
                    <option value="2">审核未通过</option>
                </select>
                </c:if>
                <label class="table_lab fyh">关键字查询</label>
                <input type="text" name="info" id="text" class="table_text" placeholder="请输入标题关键字" />
                <input style="width: 50px;" type="submit" class="table_bt fyh" value="查&nbsp;&nbsp;询"/>
            </form>
        </div>
        <div>
            <table class="tab">
                <tr>
                    <th width="5%" class="tab_th fyh">ID</th>
                    <th width="15%" class="tab_th fyh">标题</th>
                    <th width="15%" class="tab_th fyh">日期</th>
                    <th width="10%" class="tab_th fyh">类型</th>
                    <th width="10%" class="tab_th fyh">状态</th>
                    <c:if test="${userInfo.roleId==1||userInfo.roleId==2||userInfo.roleId==3}">
                    <th width="10%" class="tab_th fyh">姓名</th>
                    </c:if>
                    <c:if test="${userInfo.roleId==2||userInfo.roleId==3}">
                    <th width="10%" class="tab_th fyh">部门</th>
                    </c:if>
                    <c:if test="${userInfo.roleId!=0}">
                    <th width="10%" class="tab_th fyh">原因</th>
                    </c:if>
                    <c:if test="${userInfo.roleId!=1}">
                    <th width="10%" class="tab_th fyh">查看</th>
                    <th width="15%" class="tab_th fyh">管理</th>
                    </c:if>
                </tr>
                <c:forEach items="${page.items}" var="log" varStatus="vs">
                    <tr>
                        <td class="tab_td fyh">${(vs.index+1)+(page.currentPage-1)*5}</td>
                        <td class="tab_td fyh">${log.title}</td>

                        <td class="tab_td fyh"><fmt:formatDate value="${log.creatTime}" pattern="yyyy-MM-dd HH:mm:ss"/> </td>
                        <td class="tab_td fyh">${log.clazz.logClazzname}</td>
                        <c:if test="${log.logStatus==0}">
                            <td class="tab_td fyh">正在上报</td>
                        </c:if>
                        <c:if test="${log.logStatus==1}">
                            <td class="tab_td fyh">审核通过</td>
                        </c:if>
                        <c:if test="${log.logStatus==2}">
                            <td class="tab_td fyh">审核未通过</td>
                        </c:if>
                        <c:if test="${userInfo.roleId==1||userInfo.roleId==2||userInfo.roleId==3}">
                            <td class="tab_td fyh">${log.user.realName}</td>
                        </c:if>
                        <c:if test="${userInfo.roleId==2||userInfo.roleId==3}">
                            <td class="tab_td fyh">${log.dept.deptName}</td>
                        </c:if>
                        <c:if test="${userInfo.roleId!=0}">
                            <td class="tab_td fyh">${log.logView}</td>
                        </c:if>
                        <c:if test="${userInfo.roleId != 1}">
                        <td class="tab_td fyh"><a href="${pageContext.request.contextPath}/look_log?logId=${log.logId}"><input type="button" class="tab_button" value="查看"/></a></td>
                            <td class="tab_td fyh">
                            <c:if test="${userInfo.roleId == 0}">
                            <a href="${pageContext.request.contextPath}/editor_log?logId=${log.logId}"><input type="button" class="tab_button" value="修改"/></a>
                            </c:if>
                            <a href="${pageContext.request.contextPath}/delete_log?logId=${log.logId}" onclick="javaScripte:return del()"><input type="button" class="tab_button" value="删除"/></a></td>
                        </c:if>
                    </tr>
                </c:forEach>
            </table>
            <form action="#" class="tab_form">

                <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.totalPage}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="尾页" /></a>
                <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.currentPage+1}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="下一页" /></a>
                <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.currentPage-1}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="上一页" /></a>
                <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=1&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="首页" /></a>
                <div class=" table_page fyh">
                    共&nbsp;<span>${page.totalRows}</span>&nbsp;条记录&nbsp;&nbsp;&nbsp;&nbsp;
                每页&nbsp;<span>10</span>&nbsp;条&nbsp;&nbsp;&nbsp;&nbsp;
                页次：<input type="text" value="${page.currentPage}" class="table_pgnum"/>共${page.totalPage}&nbsp;&nbsp;页
        </div>
            </form>
        </div>
<s:debug></s:debug>
</body>
</html>


