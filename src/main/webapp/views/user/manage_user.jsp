<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/form.js"></script>
    <script type="text/javascript">
        $(function () {
            var url = "${pageContext.request.contextPath}/selectDept_user";
            $.post(url,function (data) {
                $(data).each(function (i,n) {
                    var vsId = "${model.deptId}";
                    if(vsId == n.deptId){
                        $("#deptId").append("<option value='"+n.deptId+"' selected>"+n.deptName+"</option>");
                    }else{
                        $("#deptId").append("<option value='"+n.deptId+"'>"+n.deptName+"</option>");
                    }
                });
            },"json")
        })
        function del() {
            var con="您是否要删除你的员工";
            if (confirm(con)==true){
                return true;
            }else {
                return false;
            }
        }
    </script>
</head>
<body>
<div class="tabel_choose">
<form action="${pageContext.request.contextPath}/manage_user"  method="post">
    <c:if test="${userInfo.roleId!=1}">
    <label class="table_lab fyh">部门查询</label>
    <select id="deptId" name="deptId" class="table_select2">
        <option value="" >请选择</option>
        </select>
    </c:if>
    <label class="table_lab fyh">关键字查询</label>
    <input type="text" name="info" id="text" class="table_text" placeholder="请输入姓名关键字" />
    <input style="width: 50px;" type="submit" class="table_bt fyh" value="查&nbsp;&nbsp;询"/>
</form>
</div>
<div>
    <table class="tab">
        <tr>
            <th width="5%" class="tab_th fyh">ID</th>
            <th width="15%" class="tab_th fyh">用户名</th>
            <th width="5%" class="tab_th fyh">性别</th>
            <th width="10%" class="tab_th fyh">联系方式</th>
            <th width="10%" class="tab_th fyh">姓名</th>
            <th width="10%" class="tab_th fyh">部门</th>
            <th width="5%" class="tab_th fyh">日志数</th>
            <th width="10%" class="tab_th fyh">职位</th>
            <th width="10%" class="tab_th fyh">状态</th>
            <th width="15%" class="tab_th fyh">管理</th>
        </tr>
        <c:forEach items="${page.items}" var="user" varStatus="vs">
            <tr>
                <td class="tab_td fyh"><span class="tab_span">${(vs.index+1)+(page.currentPage-1)*5}</span></td>
                <td class="tab_td fyh">${user.username}</td>
                <td class="tab_td fyh">
                <c:if test="${user.sex==0}">
                    男
                </c:if>
                    <c:if test="${user.sex==1}">
                        女
                    </c:if>
                </td>
                <td class="tab_td fyh">${user.mobile}</td>
                <td id="status"  class="tab_td fyh">${user.realName}</td>
                <td class="tab_td fyh">${user.department.deptName}</td>
                <td class="tab_td fyh">${user.logSize}</td>
                <td class="tab_td fyh">${user.role.roleName}</td>
                <td class="tab_td fyh">
                    <c:if test="${user.userStatus==0}">
                        离职
                    </c:if>
                    <c:if test="${user.userStatus==1}">
                        在职
                    </c:if>
                </td>
                <td class="tab_td fyh">
                <c:if test="${userInfo.roleId==3||userInfo.roleId==2}">
                    <a href="${pageContext.request.contextPath}/updateEmployee_user?userId=${user.userId}"><input type="button" class="tab_button" value="修改"/></a>
                </c:if>
                    <a href="${pageContext.request.contextPath}/deleteOther_user?userId=${user.userId}" onclick="javaScripte:return del()"><input type="button" class=" tab_button" value="删除"/></a></td>
                </td>
            </tr>
        </c:forEach>
    </table>
    <form action="#" class="tab_form">
        <a href="${pageContext.request.contextPath}/manage_user?CurrentPage=${page.totalPage}"><input type="button" class="table_button fyh" value="尾页" /></a>
        <a href="${pageContext.request.contextPath}/manage_user?CurrentPage=${page.currentPage+1}"><input type="button" class="table_button fyh" value="下一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_user?CurrentPage=${page.currentPage-1}"><input type="button" class="table_button fyh" value="上一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_user?CurrentPage=1"><input type="button" class="table_button fyh" value="首页" /></a>
        <div class=" table_page fyh">
            共&nbsp;<span>${page.totalRows}</span>&nbsp;条记录&nbsp;&nbsp;&nbsp;&nbsp;
            每页&nbsp;<span>10</span>&nbsp;条&nbsp;&nbsp;&nbsp;&nbsp;
            页次：<input type="text" value="${page.currentPage}" class="table_pgnum"/>共${page.totalPage}&nbsp;&nbsp;页
        </div>
    </form>
    <div class="form_zz">
        <div class="form_zzbg"></div>
        <div class="form_zzcontent">
            <h1 class="fyh form_zztit"><img src="/static/skin/images/form_zz_icon_03.png" class="form_zzimg"/>提示信息</h1>
            <div class="form_ts fyh">
                请输入密码:<input id="checkPwd" type="text" placeholder="" >
            </div>
            <div class="form_zzbutton">
                <input id="checkButton" type="button" value="确&nbsp;定" class="form_zzan1 fyh"/>
                <input type="button" value="取&nbsp;消" class="form_zzan2 fyh"/>
            </div>
        </div>
    </div>
</div>

</body>
</html>
