package com.krt.user.service.impl;

import com.krt.user.dao.RolePermissionMapper;
import com.krt.user.entity.RolePermission;
import com.krt.user.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
@Transactional
@Service("rolePermissionService")
public class RolePermissionServiceImpl implements RolePermissionService {
    @Autowired
    private RolePermissionMapper rolePermissionMapper;
    @Override
    public int addPermission(RolePermission rolePermission) {
        return rolePermissionMapper.insertSelective(rolePermission);
    }

    @Override
    public int deleteRP(RolePermission rolePermission) {
        return rolePermissionMapper.deleteRP(rolePermission);

    }
}
