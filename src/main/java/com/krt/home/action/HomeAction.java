package com.krt.home.action;

import com.krt.log.entity.Log;
import com.krt.log.service.LogService;
import com.krt.log.service.LogServiceImpl;
import com.krt.user.entity.FMenu;
import com.krt.user.entity.SMenu;
import com.krt.user.entity.User;
import com.krt.user.service.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by 陈汉帅 on 2017-12-26.
 */
public class HomeAction extends ActionSupport {
    @Autowired
    private UserService userService;
    @Autowired
    private LogService logService;
    Subject subject=SecurityUtils.getSubject();
    User user= (User) subject.getSession().getAttribute("userInfo");
    ValueStack vs = ActionContext.getContext().getValueStack();
    /**
    *@Description:首页右边的展示
    *@Data:17:14 2018-01-08
    */
    public String rd(){
        //不同职位展示的不一样
        if(user.getRoleId()==0) {
            List<Log> logList = logService.getHomeLog(user.getUserId());
            subject.getSession().setAttribute("logList",logList);
            return "rd";
        }
        if (user.getRoleId()==1){
            List<Log> logList = logService.getHomeLogByDept(user.getDeptId());
            subject.getSession().setAttribute("logList",logList);
            List<User> userList = userService.getUserByDept(user.getDeptId());
            subject.getSession().setAttribute("userList",userList);
            return "rd";
        }
        if (user.getRoleId()==2||user.getRoleId()==3){
            List<Log> logList = logService.getHomeAllLog();
            subject.getSession().setAttribute("logList",logList);
            List<User> userList = userService.getUser();
            subject.getSession().setAttribute("userList",userList);
            return "rd";
        }
        return null;
    }
    /**
    *@Description:登录成功，进入首页
    *@Data:17:15 2018-01-08
    */
    public String home(){
        if (user.getUserStatus()==0){
            return "faile";
        }
        return "home";
    }
}
