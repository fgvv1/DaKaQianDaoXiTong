package com.krt.user.service.impl;

import com.krt.user.dao.SMenuMapper;
import com.krt.user.entity.SMenu;
import com.krt.user.service.SMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
@Service("sMenuService")
public class SMenuServiceImpl implements SMenuService {
    @Autowired
    private SMenuMapper sMenuMapper;
    @Override
    public List<SMenu> selectSMenu(int roleId) {
        return sMenuMapper.selectSMenu(roleId);
    }

    @Override
    public List<SMenu> selectSMenuDelete(int roleId) {
        return sMenuMapper.selectSMenuDelete(roleId);
    }
}
