package com.krt.common;

import com.krt.log.entity.Log;

import java.util.Collections;
import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-01.
 * 分页插件
 */
public class Page {
    //一页多少记录
    private  Integer PageSize;
    //总共多少页
    private  Integer TotalPage;
    //当前页数
    private  Integer CurrentPage;
    //起始页
    private Integer StartRow;
    //总共记录数
    private int TotalRows;

    public List<?> getItems() {
        return items == null ? Collections.EMPTY_LIST : items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }

    private List<?> items;

    private final Integer DEFAULT_PAGE_SIZE=10;

    public Integer getPageSize() {

        return PageSize==null?DEFAULT_PAGE_SIZE:PageSize;
    }

    public void setPageSize(Integer pageSize) {
        PageSize = pageSize;
    }

    public Integer getTotalPage() {

        return TotalPage == null || TotalPage == 0 ? 1 : TotalPage;
    }

    public void setTotalPage(Integer totalPage) {
        TotalPage = totalPage;

    }

    public Integer getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        CurrentPage = currentPage;
    }

    public Integer getStartRow() {
        if (StartRow==null){
            //如果没有起始记录
            StartRow=(CurrentPage==null?0 :(CurrentPage-1)*getPageSize());
        }
        return StartRow;
    }

    public void setStartRow(Integer startRow) {
        StartRow = startRow;
    }

    public int getTotalRows() {

        return TotalRows;
    }

    public void setTotalRows(int totalRows) {
        TotalRows = totalRows;
        int totalpage=TotalRows%getPageSize()==0?TotalRows/getPageSize():TotalRows/getPageSize()+1;
        setTotalPage(totalpage);
    }


}

