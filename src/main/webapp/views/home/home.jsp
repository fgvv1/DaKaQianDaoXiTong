<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>首页</title>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/base_style.css"/>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/index.css"/>
    <script type="text/javascript" src="/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="/static/skin/js/index.js"></script>
</head>

<body>
<c:set value="${pageContext.request.contextPath }" var="path"></c:set>
<div class="tit_bg1">
    <div class="tit_bg2">
        <div class="tit_textlay">
            <h1 class="tit_text fyh">CRT日志管理系统</h1>&nbsp;&nbsp;CREAT&nbsp;&nbsp;&nbsp;LOG&nbsp;&nbsp;&nbsp;MANAGERIRL&nbsp;&nbsp;SYSTEM
        </div>
        <div class="time_layout fyh">欢迎<span style="color: red">${userInfo.realName}</span>登录CREAT日志管理系统！<em class="tit_time"></em></div>
        <div class="tit_button">
            <a class="tit_back"><img src="/static/skin/images/index_tit_icon_06.png" class="tit_img"/>
                <div class="back_ts fyh">用户设置</div>
            </a>
            <img src="/static/skin/images/index_tit_icon_03.png" class="tit_img"/>
            <a href="${path}/home_home" class="tit_back"><img src="/static/skin/images/index_tit_icon_09.png" class="tit_img"/>
                <div class="back_ts fyh">返回主页</div>
            </a>
            <img src="/static/skin/images/index_tit_icon_03.png" class="tit_img"/>
            <a href="${path}/logout" class="tit_back"><img src="/static/skin/images/index_tit_icon_11.png" class="tit_img"/>
                <div class="back_ts fyh">退出系统</div>
            </a>
        </div>
    </div>
</div>
<!--left-->
<div class="left_layout">
    <div class="left_manage fyh">管理菜单<img src="/static/skin/images/left_hidden_03.png" class="manage_img"/></div>
    <ul class="left_first_ul">
        <li class="left_first_li">
            <shiro:hasPermission name="日志专区"><a class="left_first_a fyh">日志专区</a></shiro:hasPermission>
            <ul class="left_second_ul">
                <shiro:hasPermission name="add_log">
                    <li><a href="add_log" target="show" class="left_third_a fyh">写日志</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="sp_log">
                    <li><a href="sp_log" target="show" class="left_third_a fyh">审批日志</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="manage_log">
                    <li><a href="manage_log" target="show" class="left_third_a fyh">管理日志</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="addLogClazz_log">
                    <li><a href="addLogClazz_log" target="show" class="left_third_a fyh">日志类型</a></li>
                </shiro:hasPermission>
            </ul>
        </li>
        <li class="left_first_li">
            <shiro:hasPermission name="用户专区"><a class="left_first_a fyh">用户专区</a></shiro:hasPermission>
            <ul class="left_second_ul">
                <shiro:hasPermission name="add_user">
                    <li><a href="add_user" target="show" class="left_third_a fyh">用户添加</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="manage_user">
                    <li><a href="manage_user" target="show" class="left_third_a fyh">用户管理</a></li>
                </shiro:hasPermission>
            </ul>
        </li>
        <li class="left_first_li">
            <shiro:hasPermission name="部门专区"><a class="left_first_a fyh">部门专区</a></shiro:hasPermission>
            <ul class="left_second_ul">

                <shiro:hasPermission name="add_dept">
                    <li><a href="add_dept" target="show" class="left_third_a fyh">部门添加</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="manage_dept">
                    <li><a href="manage_dept" target="show" class="left_third_a fyh">部门管理</a></li>
                </shiro:hasPermission>
            </ul>
        </li>
        <li class="left_first_li">
            <shiro:hasPermission name="个人信息"><a class="left_first_a fyh">个人信息</a></shiro:hasPermission>
            <ul class="left_second_ul">
                <shiro:hasPermission name="read_user">
                    <li><a href="read_user" target="show" class="left_third_a fyh">查看信息</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="update_user">
                    <li><a href="update_user" target="show" class="left_third_a fyh">修改信息</a></li>
                </shiro:hasPermission>

            </ul>
        </li>
        <li class="left_first_li">
            <shiro:hasPermission name="系统管理"><a class="left_first_a fyh">系统管理</a></shiro:hasPermission>
            <ul class="left_second_ul">
                <shiro:hasPermission name="manage_system">
                    <li><a href="manage_system" target="show" class="left_third_a fyh">权限管理</a></li>
                </shiro:hasPermission>
                <shiro:hasPermission name="add_system">
                    <li><a href="add_system" target="show" class="left_third_a fyh">分配权限</a></li>
                </shiro:hasPermission>
            </ul>
        </li>
    </ul>

</div>

<!--right-->
<div class="right_layout">
    <img src="/static/skin/images/left_hidden1_03.png" class="hidden_img"/>
    <div class="right_content">
        <div class="hr-5"></div>
        <div class="right_tit">
            <div class="right_back fyh"><a href="javascript:history.go(-1)">返回</a></div>
            <div class="tit_bg fyh"></div>
        </div>
        <iframe frameborder="0" src="rd_home" scrolling="auto" name="show" id="show" width="100%"></iframe>
    </div>
    <div class="floor fyh">软件开发：&nbsp;赣州科睿特软件技术有限公司(技术支持)&nbsp;&nbsp;&nbsp;&nbsp;客服电话：&nbsp;0797-8356742</div>
</div>
<div class="clear"></div>
</body>
</html>

