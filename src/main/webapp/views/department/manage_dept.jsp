
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        $(function(){
            $(".deptDescripte ").click(function(){
                if(!$(this).is('.input')){
                    $(this).addClass('input').html('<input id="succ"  onblur="succ()" style="border: 0px;outline:none;width: 100%;text-align: center" type="text" value="'+ $(this).text() +'" />'+'<input id="id" type="hidden" value="'+$(".deptDescripte input").val()+'"/>').find('input').focus().blur(function(){
                        $(this).parent().removeClass('input').html($(this).val() || 0);
                    });
                }
            }).hover(function(){
                $(this).addClass('hover');
            },function(){
                $(this).removeClass('hover');
            });
        });
        function succ() {
            var deptDescripte = $("#succ").val();
            var id = $("#id").val();
            var param = {"deptDesc":deptDescripte,"id":id};
            var url = "${pageContext.request.contextPath}/submitDeptDescripte_dept";
            $.post(url,param,function (data) {

            })
        }

    </script>
</head>
<body>

<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>部门类型</b></span></hr>
</div>
<div id="logFamily_manage">
    <table class="tab">
        <tr>
            <th width="5%" class="tab_th fyh">ID</th>
            <th width="10%" class="tab_th fyh">部门类型</th>
            <th width="10%" class="tab_th fyh">部门人数</th>
            <th width="20%" class="tab_th fyh">部门介绍</th>
            <th width="10%" class="tab_th fyh">管理</th>
        </tr>
        <c:forEach items="${deptList}" var="dept" varStatus="vs">
            <tr>
                <td class="tab_td fyh"><span class="tab_span">${vs.index+1}</span></td>
                <td class="tab_td fyh ">${dept.deptName}</td>
                <td class="tab_td fyh "><a style="text-decoration:none" href="${pageContext.request.contextPath}/manage_user?deptId=${dept.deptId}">${dept.userSize}</a></td>
                <td class="tab_td fyh deptDescripte">
                    ${dept.deptDescripte}
                    <input type="hidden" value="${dept.deptId}">
                </td>
                <td class="tab_td fyh">
                    <a href="${pageContext.request.contextPath}/look_dept?deptId=${dept.deptId}"><input type="button" class="tab_button" value="分配"/></a>
                    <a href="${pageContext.request.contextPath}/delete_dept?deptId=${dept.deptId}" onclick="javaScripte:return del()"><input type="button" class="tab_button" value="删除"/></a></td>
            </tr>
        </c:forEach>
    </table>
</div>

</body>
</html>

