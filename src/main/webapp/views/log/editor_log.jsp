<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=emulateIE7"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="/static/skin/css/log/style.css"/>
    <link rel="stylesheet" type="text/css" href="/static/skin/css/log/form.css"/>
    <link rel="stylesheet" type="text/css" href="/static/skin/css/base_style.css"/>
    <link rel="stylesheet" type="text/css" href="/static/skin/css/form.css"/>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/table.css"/>
    <script type="text/javascript" src="/static/skin/js/jquery-1.7.1.js"></script>
    <script src="/static/skin/js/xheditor-1.1.14-zh-cn.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#logFamily").val("${log1.logClazz}")
        })

    </script>

</head>

<body>
<c:set value="${pageContext.request.contextPath }" var="path"></c:set>
<form action="${path}update_log" method="post">
    <div id="container">
        <div id="bd">
            <div id="main">
                <div class="subfild-content base-info">
                    <div class="kv-item ue-clear">
                        <label><span class="impInfo">*</span>文章标题</label>
                        <div class="kv-item-content">
                            <input type="text" name="title" value="${log1.title}" placeholder="文章标题"/>
                            <input type="hidden" name="logId" value="${log1.logId}"/>
                        </div>
                        <span class="kv-item-tip">标题字数限制在35个字符</span>
                    </div>
                    <div class="kv-item  time">
                        <label><span class="impInfo">*</span>日志类型</label>
                        <select name="logClazz" id="logFamily" class="table_select1" style="width: 80px;">
                            <option value="">--请选择</option>
                            <c:forEach items="${logClazzList}" var="logClazz">
                                <option value="${logClazz.logClazz}">${logClazz.logClazzname}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="subfild-content remarkes-info">
                    <div class="kv-item ue-clear">
                        <label><span class="impInfo">*</span>文章内容</label>
                        <div class="kv-item-content">
                            <textarea name="context" class="xheditor {skin:'default'}"  placeholder="文章内容" id="content"
                                      style="width: 800px;height: 240px;">${log1.context}</textarea>
                        </div>
                    </div>
                </div>
                <div class="buttons fm_td5">
                    <td colspan="4">
                        <input type="submit" value="" class="table_bt"/><input type="submit" class="table_bt fyh"
                                                                               value="发&nbsp;&nbsp;表"/><input
                            type="submit" value="" class="table_bt"/>
                    </td>
                </div>
            </div>
        </div>
    </div>
</form>

</body>
</html>
