package com.krt.log.service;

import com.krt.log.dao.LogClazzMapper;
import com.krt.log.entity.LogClazz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 陈汉帅 on 2018-01-04.
 */
@Service("logClazzService")
@Transactional
public class LogClazzServiceImpl implements LogClazzService {
    @Autowired
    private LogClazzMapper logClazzMapper;
    @Override
    public int deleteLogClazz(Integer logClazz) {
        int result= logClazzMapper.deleteLogClazz(logClazz);
        return result;
    }

    @Override
    public int addClazzLogSucc(LogClazz logClazz) {

        return logClazzMapper.insertSelective(logClazz);
    }
}
