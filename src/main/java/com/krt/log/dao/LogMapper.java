package com.krt.log.dao;

import com.krt.log.entity.Log;
import com.krt.log.entity.LogClazz;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LogMapper {
    int deleteByPrimaryKey(Integer logId);

    int insert(Log record);

    int insertSelective(Log record);

    Log selectByPrimaryKey(Integer logId);

    int updateByPrimaryKeySelective(Log record);

    int updateByPrimaryKey(Log record);


    int saveLog(Log log);

    int getPersonLogSize(Integer userId);

    List<Log> selectLogByUserId(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info") String info,@Param("userId") Integer userId,@Param("startRows") Integer startRows,@Param("pageSize") Integer pageSize);

    int getTotalRowsByUserId(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info") String info,@Param("userId") Integer userId);

    int getTotalRowsByDeptId(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info") String info,@Param("deptId") Integer deptId);

    List<Log> selectLogByDeptId(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info") String info,@Param("deptId") Integer deptId,@Param("startRows") Integer startRows,@Param("pageSize") Integer pageSize);

    int getTotalRows(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info")String info);

    List<Log> selectLog(@Param("logClazz") Integer logClazz,@Param("logStatus") Integer logStatus,@Param("info") String info,@Param("startRows") Integer startRows,@Param("pageSize") Integer pageSize);

    Log lookLog(Integer logId);

    int updateLog(@Param("log") Log log);

    int getTotalRowsAsSpLog(@Param("deptId") Integer deptId,@Param("logStatus") int logStatus);

    List<Log> selectAsSpLog(@Param("deptId")Integer deptId,@Param("logStatus") int logStatus, @Param("startRows")Integer startRow,@Param("pageSize") Integer pageSize);
}