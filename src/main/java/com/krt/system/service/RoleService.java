package com.krt.system.service;

import com.krt.common.Page;
import com.krt.user.entity.Role;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
public interface RoleService {
    Page getAllRolePermission(Integer CurrentPage);
}
