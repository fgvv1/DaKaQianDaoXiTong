package com.krt.user.dao;

import com.krt.user.entity.RolePermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RolePermissionMapper {
    int insert(RolePermission record);

    int insertSelective(RolePermission record);


    int deleteRP(@Param("rolePermission") RolePermission rolePermission);
}