package com.krt.user.service;

import com.krt.user.entity.SMenu;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
public interface SMenuService {
    List<SMenu> selectSMenu(int roleId);

    List<SMenu> selectSMenuDelete(int roleId);
}
