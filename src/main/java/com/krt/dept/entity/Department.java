package com.krt.dept.entity;

public class Department {
    private Integer deptId;//部门号

    private String deptName;//部门名称

    private String deptDescripte;//部门介绍

    private int userSize;//部门的人数

    public int getUserSize() {
        return userSize;
    }

    public void setUserSize(int userSize) {
        this.userSize = userSize;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getDeptDescripte() {
        return deptDescripte;
    }

    public void setDeptDescripte(String deptDescripte) {
        this.deptDescripte = deptDescripte == null ? null : deptDescripte.trim();
    }
}