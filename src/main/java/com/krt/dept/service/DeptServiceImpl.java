package com.krt.dept.service;

import com.krt.dept.dao.DepartmentMapper;
import com.krt.dept.entity.Department;
import com.krt.user.dao.UserMapper;
import com.krt.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 陈汉帅 on 2018-01-06.
 */
@Transactional
@Service("deptService")
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DepartmentMapper deptMapper;
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<Department> getAllDept() {
        return deptMapper.getAllDept();
    }

    @Override
    public void submitDeptDescripte(String deptDescripte, int id) {
        Department department= deptMapper.selectByPrimaryKey(id);
        department.setDeptDescripte(deptDescripte);
        deptMapper.insertSelective(department);
    }

    @Override
    public int deleteDept(Department department) {
        //首先把这个部门删除
        int result= deptMapper.deleteByPrimaryKey(department.getDeptId());
        //然后把部门下的员工的部门号置为null
        userMapper.updateByDeptId(department.getDeptId());
        return result;
    }

    @Override
    public int insertDept(Department department, List<String> employee) {
        //先添加部门
        deptMapper.insertSelective(department);
        int deptId = deptMapper.selectByDtptName(department.getDeptName());
        //先获取职员
        List<Integer> employeeInt = new ArrayList<Integer>();
        for(String s:employee){
            employeeInt.add(Integer.parseInt(s));
        }
        //再往部门中添加人数
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("deptId",deptId);
        map.put("employeeInt",employeeInt);
        int result = userMapper.updateByList(map);

        return result;
    }
}
