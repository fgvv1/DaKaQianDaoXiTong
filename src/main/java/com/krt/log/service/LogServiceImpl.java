package com.krt.log.service;

import com.krt.common.Page;
import com.krt.log.dao.LogClazzMapper;
import com.krt.log.dao.LogMapper;
import com.krt.log.entity.Log;
import com.krt.log.entity.LogClazz;
import com.krt.user.entity.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-01.
 *
 */
@Transactional
@Service("logService")
public class LogServiceImpl implements LogService {
    @Autowired
    private LogClazzMapper logClazzMapper;
    @Autowired
    private LogMapper logMapper;

    @Override
    public List<LogClazz> getLogClazz() {
        return logClazzMapper.getLogClazz();
    }

    @Override
    public int saveLog(Log log) throws ParseException {
        //获取session对象
        Subject subject= SecurityUtils.getSubject();
        User user= (User) subject.getSession().getAttribute("userInfo");
        //将一些信息封装到log，再插入log表
        log.setCreatTime(new Date());
        log.setDeptId(user.getDeptId());
        log.setUserId(user.getUserId());
        log.setLogStatus(0);
        return logMapper.saveLog(log);
    }

    @Override
    public Page manageLog(Integer logClazz, Integer logStatus, String info,Integer CurrentPage) {
        Page page=new Page();
        Subject subject= SecurityUtils.getSubject();
        User user= (User) subject.getSession().getAttribute("userInfo");
        //职位不同，取出的日志也不一样
        if (user.getRoleId()==0){
            page.setTotalRows(logMapper.getTotalRowsByUserId(logClazz,logStatus,info,user.getUserId()));
            //如果当前页数大于总页数，则
            CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
            CurrentPage=(CurrentPage<=0)?1:CurrentPage;
            page.setCurrentPage(CurrentPage);
            page.setItems(logMapper.selectLogByUserId(logClazz,logStatus,info,user.getUserId(),page.getStartRow(),page.getPageSize()));
            return page;
        }
        if (user.getRoleId()==1){
            page.setTotalRows(logMapper.getTotalRowsByDeptId(logClazz,logStatus,info,user.getDeptId()));
            //如果当前页数大于总页数，则
            CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
            CurrentPage=(CurrentPage<=0)?1:CurrentPage;
            page.setCurrentPage(CurrentPage);
            page.setItems(logMapper.selectLogByDeptId(logClazz,logStatus,info,user.getDeptId(),page.getStartRow(),page.getPageSize()));
            return page;
        }
        if (user.getRoleId()==2||user.getRoleId()==3){
            page.setTotalRows(logMapper.getTotalRows(logClazz,logStatus,info));
            //如果当前页数大于总页数，则
            CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
            CurrentPage=(CurrentPage<=0)?1:CurrentPage;
            page.setCurrentPage(CurrentPage);
            page.setCurrentPage(CurrentPage);
            page.setItems(logMapper.selectLog(logClazz,logStatus,info,page.getStartRow(),page.getPageSize()));
            return page;
        }
        return null;
    }
/**
*@Description:查看具体日志
*@Data:19:03 2018-01-03
*/
    @Override
    public Log lookLog(Integer logId) {
        return logMapper.lookLog(logId);
    }
/**
*@Description:更新日志操作
*@Data:20:34 2018-01-03
*/
    @Override
    public int updateLog(Log log) {
        Subject subject= SecurityUtils.getSubject();
        User user= (User) subject.getSession().getAttribute("userInfo");
        //获取更新前的时间断，在赋值给新的时间
        Log log1= logMapper.lookLog(log.getLogId());
        log.setCreatTime(log1.getCreatTime());
        log.setDeptId(user.getDeptId());
        log.setUserId(user.getUserId());
        log.setLogStatus(0);
        log.setLogView(null);
        return logMapper.updateByPrimaryKey(log);
    }
/**
*@Description:删除日志操作
*@Data:20:34 2018-01-03
*/
    @Override
    public int deleteLog(Integer logId) {
        return logMapper.deleteByPrimaryKey(logId);
    }

    @Override
    public Page spLog(Integer CurrentPage ) {
        Subject subject= SecurityUtils.getSubject();
        User user= (User) subject.getSession().getAttribute("userInfo");
        Page page=new Page();
        page.setTotalRows(logMapper.getTotalRowsAsSpLog(user.getDeptId(),0));
        //如果当前页数大于总页数，则
        CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
        page.setCurrentPage(CurrentPage);
        page.setItems(logMapper.selectAsSpLog(user.getDeptId(),0,page.getStartRow(),page.getPageSize()));
        return page;
    }
    /**
    *@Description:审批日志成功
    *@Data:9:12 2018-01-04
    */
    @Override
    public int spUpdateLog(Log log) {
        Subject subject= SecurityUtils.getSubject();
        User user= (User) subject.getSession().getAttribute("userInfo");
        //获取更新前的时间断，在赋值给新的时间
        Log log1= logMapper.lookLog(log.getLogId());
        log1.setLogStatus(log.getLogStatus());
        log1.setLogView(log.getLogView());
        return logMapper.updateByPrimaryKeySelective(log1);
    }

    @Override
    public List<Log> getHomeLog(int userId) {
        return logMapper.selectLogByUserId(null,null,null,userId,0,5);
    }

    @Override
    public List<Log> getHomeLogByDept(Integer deptId) {
        return logMapper.selectAsSpLog(deptId,0,0,5);
    }

    @Override
    public List<Log> getHomeAllLog() {
        return logMapper.selectLog(null,null,null,0,5);
    }


}
