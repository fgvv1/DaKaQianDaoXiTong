package com.krt.login.action;

import com.alibaba.fastjson.JSON;
import com.krt.user.entity.FMenu;
import com.krt.user.entity.SMenu;
import com.krt.user.entity.User;
import com.krt.user.service.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 *
 * Created by Administrator on 2017-12-22.
 */

public class LoginAction extends ActionSupport implements ModelDriven<User>{
    private User user=new User();
    @Autowired
    private UserService userService;
    /**
    *@Description:登录功能
    *@Data:21:26 2017-12-22
    */
    public String login(){
        UsernamePasswordToken token=new UsernamePasswordToken(user.getUsername(),user.getPassword());
        Subject subject= SecurityUtils.getSubject();
        //调用shiro登录认证
        subject.login(token);
        //登录成功，查询home需要的功能
        return SUCCESS;
    }
    public User getModel(){
        return user;
    }


/**
*@Description:退出登录
*@Data:17:54 2017-12-29
*/
    public String logout(){
        ServletActionContext.getRequest().getSession().invalidate();
        return "logout";
    }
}
