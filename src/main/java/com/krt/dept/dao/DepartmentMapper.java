package com.krt.dept.dao;

import com.krt.dept.entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DepartmentMapper {
    int deleteByPrimaryKey(Integer deptId);

    int insert(Department record);

    int insertSelective(Department record);

    Department selectByPrimaryKey(Integer deptId);

    int updateByPrimaryKeySelective(Department record);

    int updateByPrimaryKey(Department record);

    List<Department> selectDept();


    List<Department> getAllDept();

    void insertByDeptId(@Param("deptDescripte") String deptDescripte,@Param("id") int id);

    int selectByDtptName(String deptName);
}