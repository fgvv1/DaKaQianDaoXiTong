<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>权限管理</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script >
    </script>
</head>
<body>

<div>
    <table class="tab">
        <tr>
            <th width="5%" class="tab_th fyh">ID</th>
            <th width="15%" class="tab_th fyh">角色</th>
            <th width="15%" class="tab_th fyh">模块</th>
            <th width="15%" class="tab_th fyh">权限</th>
            <th width="10%" class="tab_th fyh">管理</th>
        </tr>
        <c:forEach items="${page.items}" var="role" varStatus="vs">
            <c:forEach items="${role.sMenuList}" var="smenu" >
            <tr>
                <td class="tab_td fyh">${(vs.index+1)+(page.currentPage-1)*5}</td>
                <td class="tab_td fyh">${role.roleName}</td>
                <td class="tab_td fyh">${smenu.fmenu.fName}</td>
                <td class="tab_td fyh">${smenu.sname}</td>
                <td class="tab_td fyh">
                    <a href="${pageContext.request.contextPath}/delete_system?roleId=${role.roleId}&sMenuId=${smenu.smenuId}" onclick="javaScripte:return del()"><input type="button" class="tab_button" value="删除"/></a></td>
            </tr>
                </c:forEach>
        </c:forEach>
    </table>
    <form action="#" class="tab_form">

        <a href="${pageContext.request.contextPath}/manage_system?CurrentPage=${page.totalPage}"><input type="button" class="table_button fyh" value="尾页" /></a>
        <a href="${pageContext.request.contextPath}/manage_system?CurrentPage=${page.currentPage+1}"><input type="button" class="table_button fyh" value="下一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_system?CurrentPage=${page.currentPage-1}"><input type="button" class="table_button fyh" value="上一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_system?CurrentPage=1&logClazz=${logClazz}"><input type="button" class="table_button fyh" value="首页" /></a>
        <div class=" table_page fyh">
            共&nbsp;<span>${page.totalRows}</span>&nbsp;条记录&nbsp;&nbsp;&nbsp;&nbsp;
            每页&nbsp;<span>10</span>&nbsp;条&nbsp;&nbsp;&nbsp;&nbsp;
            页次：<input type="text" value="${page.currentPage}" class="table_pgnum"/>共${page.totalPage}&nbsp;&nbsp;页
        </div>
    </form>
</div>

<s:debug></s:debug>
</body>
</html>


