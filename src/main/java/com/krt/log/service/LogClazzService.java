package com.krt.log.service;

import com.krt.log.entity.LogClazz;

/**
 * Created by 陈汉帅 on 2018-01-04.
 */
public interface LogClazzService {
    int deleteLogClazz(Integer logClazz);

    int addClazzLogSucc(LogClazz logClazz);
}
