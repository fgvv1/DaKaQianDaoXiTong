package com.krt.log.entity;

import com.krt.dept.entity.Department;
import com.krt.user.entity.User;

import java.util.Date;

public class Log {
    private Integer logId;//日志id

    private Integer logClazz;//日志类型id

    private String title;//日志的标题

    private String context;//日志上下文

    private Date creatTime;//日志创建的时间

    private Integer logStatus;//日志的状态

    private Integer userId;//写日志的员工的id

    private Integer deptId;//写日志的员工部门id

    private String logView;//部长审批的意见

    private  LogClazz clazz;//日志类型对象，用于多表连接

    private String  LogStatusName;//日志状态的名称

    private Department dept;//部门对象，多表连接

    private User user;//user对象，多表连接

    private int logSize;//写日志的总数量

    public int getLogSize() {
        return logSize;
    }

    public void setLogSize(int logSize) {
        this.logSize = logSize;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Department getDept() {
        return dept;
    }

    public void setDept(Department dept) {
        this.dept = dept;
    }

    public String getLogStatusName() {
        return LogStatusName;
    }

    public void setLogStatusName(String logStatusName) {
        LogStatusName = logStatusName;
    }

    public LogClazz getClazz() {
        return clazz;
    }

    public void setClazz(LogClazz clazz) {
        this.clazz = clazz;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Integer getLogClazz() {
        return logClazz;
    }

    public void setLogClazz(Integer logClazz) {
        this.logClazz = logClazz;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context == null ? null : context.trim();
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Integer getLogStatus() {
        return logStatus;
    }

    public void setLogStatus(Integer logStatus) {
        this.logStatus = logStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getLogView() {
        return logView;
    }

    public void setLogView(String logView) {
        this.logView = logView == null ? null : logView.trim();
    }
}