<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>首页-右部</title>
    <link type="text/css" rel="stylesheet" href="/static/skin/css/base_style.css" />
    <link type="text/css" rel="stylesheet" href="/static/skin/css/right_index.css" />
    <script type="text/javascript" src="/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="/static/skin/js/rd.js"></script>
</head>

<body>
<div class="rd_tit">
    <ul class="rd_tit_ul">
        <li class="rd_tit_li">
            <a href="${pageContext.request.contextPath}/manage_log" class="rd_tit_a fyh"><img src="/static/skin/images/right_index_icon_03.png" /><h3>管理日志</h3></a>
        </li>
        <li  class="rd_tit_li"><a href="${pageContext.request.contextPath}/read_user" class="rd_tit_a fyh"><img src="/static/skin/images/right_index_icon_05.png" /><h3>个人信息</h3></a></li>
        <li class="rd_tit_li"><a href="${pageContext.request.contextPath}/update_user" class="rd_tit_a fyh"><img src="/static/skin/images/right_index_icon_07.png" /><h3>修改信息</h3></a></li>
    </ul>
</div>
<div class="rd_content">
    <div class="rd_firstpart">
        <div class="rd_firstpart_tit">
            <div class="rd_firstPart_tit_text1"><a href="${pageContext.request.contextPath}/manage_log" class="rd_firstPart_tit_a">更多</a><img src="/static/skin/images/right_img_03.png"  class="rd_firstPart_tit_image"/></div>
            <div class="rd_firstPart_tit_text2"><img src="/static/skin/images/right_img_06.png" class="rd_firstPart_tit_image"/><span class="rd_firstPart_tit_span">日志专区</span></div>
        </div>
        <ul class="rd_firstpart_ul">
            <c:forEach items="${logList}" var="log">
            <li><a href="${pageContext.request.contextPath}/look_log?logId=${log.logId}" class="rd_firstPart_li"><div class="rd_firstPart_li_div"><fmt:formatDate value="${log.creatTime}" pattern="yyyy-MM-dd"/></div>${log.title}</a></li>
            <li class="rd_firstPart_line"></li>
            </c:forEach>
        </ul>
    </div>
    <c:if test="${userInfo.roleId!=0}">
    <div class="rd_firstpart">
        <div class="rd_firstpart_tit">
            <div class="rd_firstPart_tit_text1"><a href="${pageContext.request.contextPath}/manage_user" class="rd_firstPart_tit_a">更多</a><img src="/static/skin/images/right_img_03.png"  class="rd_firstPart_tit_image"/></div>
            <div class="rd_firstPart_tit_text2"><img src="/static/skin/images/right_img_06.png" class="rd_firstPart_tit_image"/><span class="rd_firstPart_tit_span">用户管理</span></div>
        </div>
        <ul class="rd_firstpart_ul">
            <c:forEach items="${userList}" var="user">
                <li><a  class="rd_firstPart_li"><div class="rd_firstPart_li_div">
                    <c:if test="${user.userStatus==0}">离职</c:if>
                    <c:if test="${user.userStatus==1}">在职</c:if>
                </div>${user.realName}</a></li>
                <li class="rd_firstPart_line"></li>
            </c:forEach>
        </ul>
    </div>
    </c:if>
    <div class="clear"></div>
</div>
</body>
</html>
