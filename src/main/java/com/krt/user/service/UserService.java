package com.krt.user.service;

import com.krt.common.Page;
import com.krt.dept.entity.Department;
import com.krt.user.entity.User;

import java.util.List;

/**
 * Created by Administrator on 2017-12-22.
 */
public interface UserService {
    User checkLogin(String username);

    User findByUser(String username);

    int updateUserInfo(User user);

    Page manageUser(Integer deptId,String info,Integer currentPage);

    int updatePwdUserInfo(User user);

    User updateEmployeeUserInfo(User user);

    List<Department> selectDept();

    int updateEmployeeSucc(User user);

    int addUser(User user);

    List<User> selectUserByNoDept();

    List<User> getUserByDept(Integer deptId);

    List<User> getUser();
}
