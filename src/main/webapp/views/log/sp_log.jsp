<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>我的日志</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>

    <script >

    </script>
</head>
<body>
<div>
    <table class="tab">
        <tr>
            <th width="5%" class="tab_th fyh">ID</th>
            <th width="15%" class="tab_th fyh">标题</th>
            <th width="15%" class="tab_th fyh">日期</th>
            <th width="10%" class="tab_th fyh">类型</th>
            <th width="10%" class="tab_th fyh">状态</th>
            <th width="10%" class="tab_th fyh">姓名</th>
            <th width="10%" class="tab_th fyh">查看</th>
        </tr>
        <c:forEach items="${page.items}" var="log" varStatus="vs">
            <tr>
                <td class="tab_td fyh">${(vs.index+1)}</td>
                <td class="tab_td fyh">${log.title}</td>
                <td class="tab_td fyh"><fmt:formatDate value="${log.creatTime}" pattern="yyyy-MM-dd HH:mm:ss"/> </td>
                <td class="tab_td fyh">${log.clazz.logClazzname}</td>
                <td class="tab_td fyh">正在上报</td>
                <td class="tab_td fyh">${log.user.realName}</td>
                <td class="tab_td fyh"><a href="${pageContext.request.contextPath}/look_log?logId=${log.logId}"><input type="button" class="tab_button" value="查看"/></a></td>
            </tr>
        </c:forEach>
    </table>
    <form action="#" class="tab_form">

        <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.totalPage}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="尾页" /></a>
        <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.currentPage+1}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="下一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=${page.currentPage-1}&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="上一页" /></a>
        <a href="${pageContext.request.contextPath}/manage_log?CurrentPage=1&logClazz=${logClazz}&logStatus=${logStatus}&info=${info}"><input type="button" class="table_button fyh" value="首页" /></a>
        <div class=" table_page fyh">
            共&nbsp;<span>${page.totalRows}</span>&nbsp;条记录&nbsp;&nbsp;&nbsp;&nbsp;
            每页&nbsp;<span>10</span>&nbsp;条&nbsp;&nbsp;&nbsp;&nbsp;
            页次：<input type="text" value="${page.currentPage}" class="table_pgnum"/>共${page.totalPage}&nbsp;&nbsp;页
        </div>
    </form>
</div>
<s:debug></s:debug>
</body>
</html>



