package com.krt.log.dao;

import com.krt.log.entity.LogClazz;

import java.util.List;

public interface LogClazzMapper {
    int insert(LogClazz record);

    int insertSelective(LogClazz record);

    List<LogClazz> getLogClazz();

    int deleteLogClazz(Integer logClazz);
}