package com.krt.user.dao;

import com.krt.user.entity.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleId);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleId);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> getAllRolePermission(@Param("startRows") Integer startRows,@Param("pageSize") Integer pageSize);

    int getAllRolePermissionSize();
}