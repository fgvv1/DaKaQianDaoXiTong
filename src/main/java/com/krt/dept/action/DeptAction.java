package com.krt.dept.action;

import com.krt.dept.entity.Department;
import com.krt.dept.service.DeptService;
import com.krt.user.entity.User;
import com.krt.user.service.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-06.
 */
/**
*@Description:部门功能
*@Data:16:18 2018-01-06
*/
public class DeptAction extends ActionSupport implements ModelDriven<Department>{
    @Autowired
    private DeptService deptService;
    @Autowired
    private UserService userService;
    //模型驱动
    private Department department=new Department();

    private List<String> employee;

    public List<String> getEmployee() {
        return employee;
    }

    public void setEmployee(List<String> employee) {
        this.employee = employee;
    }
    /**
    *@Description:管理部门
    *@Data:17:11 2018-01-08
    */
    public String manage(){
        //得到所有的部门
        List<Department> deptList = deptService.getAllDept();
        ValueStack vs = ActionContext.getContext().getValueStack();
        //放入值栈
        vs.set("deptList",deptList);
        return "manage";
    }
    /**
    *@Description:删除一个部门，并且下面的所有员工部门信息置为null
    *@Data:21:04 2018-01-06
    */
    public String delete(){
        int result = deptService.deleteDept(department);
        if (result>0){
            return "delete";
        }
        return "faile";
    }
    /**
    *@Description:增加部门
    *@Data:21:23 2018-01-06
    */
    public String add(){
        //获取没有部门的员工，然后分配部门
        List<User> userList = userService.selectUserByNoDept();
        ValueStack vs = ActionContext.getContext().getValueStack();
        vs.set("userList",userList);
        return "add";
    }
    public String addSucc(){
        int result=deptService.insertDept(department,employee);
        if (result>0){
            return "addSucc";
        }
        return "faile";
    }

    /**
    *@Description:ajax获取前台的部门介绍
    *@Data:19:42 2018-01-06
    */
    public void submitDeptDescripte(){
        String deptDescripte = (String) ServletActionContext.getRequest().getParameter("deptDesc");
        int id = Integer.parseInt(ServletActionContext.getRequest().getParameter("id"));
         deptService.submitDeptDescripte(deptDescripte,id);
    }

    @Override
    public Department getModel() {
        return department;
    }
}
