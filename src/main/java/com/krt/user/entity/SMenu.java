package com.krt.user.entity;

import java.util.List;

public class SMenu {
    private Integer smenuId;//二菜单的id

    private Integer fmenuId;//一菜单的id

    private String sname;//二菜单的名称

    private String adress;//二菜单所对应的action

    public FMenu getFmenu() {
        return fmenu;
    }

    public void setFmenu(FMenu fmenu) {
        this.fmenu = fmenu;
    }

    private FMenu fmenu;

    public Integer getSmenuId() {
        return smenuId;
    }

    public void setSmenuId(Integer smenuId) {
        this.smenuId = smenuId;
    }

    public Integer getFmenuId() {
        return fmenuId;
    }

    public void setFmenuId(Integer fmenuId) {
        this.fmenuId = fmenuId;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }


}