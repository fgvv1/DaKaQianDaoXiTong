<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018-01-04
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css" />
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript">
         function check() {
             var mobile=$("#tl").val();
             if(mobile.length!=11){
                 alert("号码长度有错");
                 return false;
             }
         }
        function checkName() {
            var url = "${pageContext.request.contextPath}/checkUsername_user";
            var username = $("#username").val();
            if(username=="${userInfo.username}"){
                return true;
            }
            $.ajax({
                type:"post",
                data:{"name":username},
                async:false,
                url:url,
                success:function (data) {
                    if(data!="true"){
                        alert("账号已经被使用");
                    }
                }
            })
        }
    </script>

</head>
<body>

<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>修改资料</b></span></hr>
</div>
<form action="${pageContext.request.contextPath}/updateSucc_user" method="post" onsubmit="return check()">
    <table class="fm_tab">
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">姓名：</span></td>
            <td class="fm_td2"><input  readonly type="text" value="${userInfo.realName}" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">用户名：</span></td>
            <td class="fm_td2"><input id="username" name="username" onblur="checkName()" type="text" value="${userInfo.username}" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">手机号码：</span></td>
            <td class="fm_td2"><input id="tl" name="mobile" type="text" value="${userInfo.mobile}" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">个人介绍：</span></td>
            <td width="35%" class="fm_td2"><input type="text" name="introduction" value="${userInfo.introduction}"  placeholder="这个人很懒，什么也没有留下"    class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">密码验证：</span></td>
            <td width="35%" class="fm_td2"><input type="text" id="old_pwd"   name="old_pwd"  class="fm_tt1" style="width:25%"/></td>
        </tr>

    </table>
    <div style="padding-top: 20px;padding-left: 300px;">
        <input style="width: 80px;"  type="submit" id="submit_id" class="table_bt fyh submit_id" value="修&nbsp;&nbsp;改"/>
    </div>
</form>
    <div style="padding-top: 20px;padding-left: 300px;">
        <a href="${pageContext.request.contextPath}tzUpdatePwd_user"><input style="width: 120px;"  type="submit" id="" class="table_bt fyh submit_id" value="修&nbsp;&nbsp;改&nbsp;&nbsp;密&nbsp;&nbsp;码"/></a>
    </div>


</body>
</html>
