<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        $(function () {
            if(${userInfo.roleId==0}){
                if("${log1.logView}".length>0){
                    alert("审核建议：${log1.logView}");
                }
            }
        })
    </script>
    <style type="text/css">
        body{background-color:#F9F9F9;}
        a{text-decoration:none;}
        a:hover{color:white;font-weight:bold;}
        .div_text{height:25px;width:67%;font-size:15px;line-height:25px;margin-left:10%;
            background-color:white;border:1px solid #cfd4d7}
        .text1_css{background-color:#e1e5ee;font-weight:bold;margin-top:15px;}
        .addAnndiv{margin-left:0%;margin-top:20px;}
        .finally_a{display:inline-block;height:23px;  line-height:23px;
            text-align:center;padding: 3px 10px;border: 1px solid #dfdede; width:70px;
            cursor: pointer;font-size:16px;margin:20px 20px 0 0px;margin-left:10%;}
        .log_text{height:240px;margin-top:20px;}
        .span1class{font-weight:bold;margin-left:5px;}
        .span2class{font-weight:bold;margin-left:5px;}
        .div_text_div{font-size:14px;margin:10px 5%;padding:0px 5%;border-left:1px solid #dfdede;
            border-right:1px solid #dfdede;height:180px;width:80%}
        .formfirstclass{width:50%;height: 20px;margin-bottom:20px;margin-left:5px;}
    </style>
</head>

<body>
<div>
    <form action="${pageContext.request.contextPath}/spSucc_log" method="post">
    <div class="div_text text1_css">&nbsp;&nbsp;操作说明</div>
    <div class="div_text">&nbsp;&nbsp;日志内容</div>
    <div class="addAnndiv">
        <div class="div_text">
            <span class="span1class">日志主题 : </span>
            <span class="span2class"  >${log1.title}</span>
            <input type="hidden" name="logId" value="${log1.logId}"/>
        </div>
        <div class="div_text divclass" style="border-top:0px;">
            <span class="span1class">日志类型 : </span>
            <span class="span2class">${log1.clazz.logClazzname}</span>
        </div>
        <div class="div_text log_text">
            <span class="span1class">日志内容 : </span>
            <div class="div_text_div">${log1.context}</div>
        </div>
        <c:if test="${userInfo.roleId==1}">
            <label style="padding-left: 10%;"></label>
        <select id="logStatus" name="logStatus" class="table_select2">
            <option value="1">审核通过</option>
            <option value="2">审核未通过</option>
        </select>
        <input type="text" name="logView" maxlength="60" class="formfirstclass" placeholder="请填写审批理由" />
        <input type="submit" class="table_bt fyh" style="width: 80px;" value="审&nbsp;&nbsp;核"/>
        </c:if>
    </div>
    </form>
</div>
</body>
</html>
