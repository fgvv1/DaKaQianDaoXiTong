package com.krt.user.dao;

import com.krt.user.entity.SMenu;

import java.util.List;

public interface SMenuMapper {
    int deleteByPrimaryKey(Integer sMenuId);

    int insert(SMenu record);

    int insertSelective(SMenu record);

    SMenu selectByPrimaryKey(Integer sMenuId);

    int updateByPrimaryKeySelective(SMenu record);

    int updateByPrimaryKey(SMenu record);

    List<SMenu> selectSMenu(int roleId);

    List<SMenu> selectSMenuDelete(int roleId);
}