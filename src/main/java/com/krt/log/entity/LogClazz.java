package com.krt.log.entity;

public class LogClazz {
    private Integer logClazz;//log类型id

    private String logClazzname;//log类型名称

    public Integer getLogClazz() {
        return logClazz;
    }

    public void setLogClazz(Integer logClazz) {
        this.logClazz = logClazz;
    }

    public String getLogClazzname() {
        return logClazzname;
    }

    public void setLogClazzname(String logClazzname) {
        this.logClazzname = logClazzname == null ? null : logClazzname.trim();
    }
}