<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>个人信息</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css" />
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        $(function () {
            if("${result}"!=0){
                alert("操作成功");
            }
        })
    </script>
</head>
<body>
        <div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
            <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>基本资料</b></span></hr>
        </div>
        <table class="fm_tab">
            <tr>
                <td class="fm_td1 fyh"><span class="fm_text">用户名：</span></td>
                <td class="fm_td2"><input id="tl" readonly name="tl" type="text" value="${userInfo.username}" class="fm_tt1" style="width:20%"/></td>
            </tr>
            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">性别：</span></td>
                <td width="35%" class="fm_td2">
                    <c:if test="${userInfo.sex==0}">
                        男
                    </c:if>
                    <c:if test="${userInfo.sex==1}">
                        女
                    </c:if>

                </td>
            </tr>
            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">电话：</span></td>
                <td width="35%" class="fm_td2"><input value="${userInfo.mobile}" readonly type="text"   class="fm_tt1" style="width:20%"/></td>
            </tr>
            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">姓名：</span></td>
                <td width="35%" class="fm_td2"><input type="text" value="${userInfo.realName}" readonly     class="fm_tt1" style="width:20%"/></td>
            </tr>
            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">职位：</span></td>
                <td width="35%" class="fm_td2"><input type="text" value="${userInfo.role.roleName}" readonly     class="fm_tt1" style="width:20%"/></td>
            </tr>
            <c:if test="${userInfo.roleId==0||userInfo.roleId==1}">
            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">部门：</span></td>
                <td width="35%" class="fm_td2"><input type="text" value="${userInfo.department.deptName}" readonly    class="fm_tt1" style="width:20%"/></td>
            </tr>
            </c:if>

            <tr>
                <td width="15%" class="fm_td1 fyh"><span class="fm_text">个人介绍：</span></td>
                <td width="35%" class="fm_td2"><input type="text" value="${userInfo.introduction}" readonly  placeholder="这个人很懒，什么也没有留下"    class="fm_tt1" style="width:40%"/></td>
            </tr>

            <tr>
                <td width="15%" class="fm_td1 fyh">
                    <span class="fm_text"></span>
                </td>
                <td width="35%" class="fm_td2">
                <a href="${pageContext.request.contextPath}/update_user">
                    <input type="submit" value="修&nbsp;&nbsp;改&nbsp;&nbsp;信&nbsp;&nbsp;息" class="table_bt submit1"/>
                </a>
                </td>
            </tr>
        </table>


</body>
</html>
