package com.krt.user.dao;

import com.krt.user.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User checkLogin(@Param("username")String username);

    User findByUser(String username);

    List<User> test(@Param("realName") String realName,@Param("deptName") String deptName);

    int getTotalUserSizeByDept(@Param("deptId") Integer deptId,@Param("info") String info);

    List<User> selectUserByDept(@Param("deptId") Integer deptId, @Param("info") String info, @Param("startRow") Integer startRow,@Param("pageSize") Integer pageSize);

    int getTotalUserSize(@Param("deptId") Integer deptId,@Param("info") String info);

    List<User> selectUser(@Param("deptId") Integer deptId,@Param("info") String info, @Param("startRow") Integer startRow, @Param("pageSize") Integer pageSize);

    void updateByDeptId(Integer deptId);

    List<User> selectUserByNoDept();

    List<User> selectUserByUserListId(List<Integer> employeeInt);

    int updateByList(Map<String, Object> map);
}