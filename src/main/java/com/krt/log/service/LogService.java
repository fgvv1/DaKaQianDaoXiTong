package com.krt.log.service;

import com.krt.common.Page;
import com.krt.log.entity.Log;
import com.krt.log.entity.LogClazz;

import java.text.ParseException;
import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-01.
 */
public interface LogService {
    List<LogClazz> getLogClazz();

    int saveLog(Log log) throws ParseException;

    Page manageLog(Integer logClazz, Integer logStatus, String info,Integer CurrentPage);

    Log lookLog(Integer logId);

    int updateLog(Log log);

    int deleteLog(Integer logId);

    Page spLog(Integer CurrentPage);

    int spUpdateLog(Log log);

    List<Log> getHomeLog(int userId);

    List<Log> getHomeLogByDept(Integer deptId);

    List<Log> getHomeAllLog();
}
