
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/form.js"></script>
    <script type="text/javascript">
        function checkname() {
            var logClazzname = $("#logClazzname").val();
            if(logClazzname.length>0){
                $(".log").each(function (i,n) {
                    if(n.innerHTML==logClazzname){
                        alert("类型以存在");
                    }
                })
            }

        }
        function checkAll(name) {
            var el = document.getElementsByTagName('input');
            var len = el.length;
            for (var i = 0; i < len; i++) {
                if ((el[i].type == "checkbox") && (el[i].name == name)) {
                    el[i].checked = true;
                }
            }
        }
        function cleanAll(name) {
            var el = document.getElementsByTagName('input');
            var len = el.length;
            for(var i=0; i<len; i++) {
                if((el[i].type=="checkbox") && (el[i].name==name)) {
                    el[i].checked = false;
                }
            }
        }
        $(function () {
            $("#checkButton").click(function () {
                var pwd =$("#checkPwd").val();
                if(pwd=="${userInfo.password}"){
                    $("#submit").submit();
                }else {
                    alert("密码错误");
                }
            })
        })

    </script>


</head>
<body>

<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>部门类型</b></span></hr>
</div>
<form action="${pageContext.request.contextPath}/addSucc_dept" method="post" id="submit">
    <table class="fm_tab">
        <tr>
            <td class="fm_td1 fyh"><img src="/static/skin/images/form_img_03.png" class="fm_mid"/><span class="fm_text">部门名称：</span></td>
            <td class="fm_td2"><input type="text" name="deptName" value="" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">部门介绍：</span></td>
            <td class="fm_td2"><textarea name="deptDescripte" style="width: 25%;height: 40px;"></textarea></td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">分配职员：</span></td>
            <td width="35%" class="fm_td2">
                <c:forEach items="${userList}" var="user">
                    <input name="employee" type="checkbox" value="${user.userId}">${user.realName}<br>
                </c:forEach>
                <input onclick="checkAll('employee')" type="button" value="全选">
                <input onclick="cleanAll('employee')" type="button" value="不选">
            </td>
        </tr>

    </table>
    <div style="padding-top: 20px;padding-left: 300px;">
        <input style="width: 120px;"  type="button" id="submit_id" class="fm_button fyh submit_id" value="添&nbsp;&nbsp;加"/>
    </div>
</form>

<div class="form_zz">
    <div class="form_zzbg"></div>
    <div class="form_zzcontent">
        <h1 class="fyh form_zztit"><img src="/static/skin/images/form_zz_icon_03.png" class="form_zzimg"/>提示信息</h1>
        <div class="form_ts fyh">
            请输入密码:<input id="checkPwd" type="text" placeholder="" >
        </div>
        <div class="form_zzbutton">
            <input id="checkButton" type="button" value="确&nbsp;定" class="form_zzan1 fyh"/>
            <input type="button" value="取&nbsp;消" class="form_zzan2 fyh"/>
        </div>
    </div>
</div>



</body>
</html>

