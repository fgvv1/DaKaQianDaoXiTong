<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018-01-04
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/del.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        function checkname() {
            var logClazzname = $("#logClazzname").val();
            if(logClazzname.length>0){
                $(".log").each(function (i,n) {
                    if(n.innerHTML==logClazzname){
                        alert("类型以存在");
                    }
                })
            }
        }


    </script>
</head>
<body>

<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>日志类型</b></span></hr>
</div>
<div id="logFamily_manage">
    <table class="tab">
        <tr>
            <th width="10%" class="tab_th fyh">ID</th>
            <th width="20%" class="tab_th fyh">日志类型</th>
            <th width="10%" class="tab_th fyh">管理</th>
        </tr>
        <c:forEach items="${logClazzList}" var="logClazz" varStatus="vs">
            <tr>
                <td class="tab_td fyh"><span class="tab_span">${vs.index+1}</span></td>
                <td class="tab_td fyh log">${logClazz.logClazzname}</td>
                <td class="tab_td fyh"><a href="${pageContext.request.contextPath}/deleteLogClazz_log?logClazz=${logClazz.logClazz}" onclick="javaScripte:return del()"><input type="button" class="tab_button" value="删除"/></a></td>
            </tr>
        </c:forEach>
    </table>
    <form action="${pageContext.request.contextPath}/addClazzLogSucc_log" method="post" >
        <div>
            <span style="font-size: 16px;padding-top: 2px;">文档类型:</span>
            <input type="text" id="logClazzname" name="logClazzname" onblur="checkname()" style="border: solid;border-width: 1px;height: 25px;">
            <input type="submit" value="添&nbsp;&nbsp;加" class="table_bt"/>
        </div>
        <input type="hidden" name="token" value="${token}"/>
    </form>
</div>

</body>
</html>
