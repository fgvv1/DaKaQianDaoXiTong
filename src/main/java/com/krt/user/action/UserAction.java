package com.krt.user.action;

import com.alibaba.fastjson.JSON;
import com.krt.common.Page;
import com.krt.dept.entity.Department;
import com.krt.user.entity.User;
import com.krt.user.service.UserService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.util.ValueStack;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-04.
 */
public class UserAction extends ActionSupport implements ModelDriven<User> {
    @Autowired
    private UserService userService;

    private Integer CurrentPage=1;

    public Integer getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        CurrentPage = currentPage;
    }

    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    private User user =new User();

    private String name;//接收前台传递的username；

    public void setName(String name) {
        this.name = name;
    }

    /**
*@Description:查询自己的信息
*@Data:17:20 2018-01-04
*/
    public String  read(){

        return "read";
    }
    /**
    *@Description:更新信息
    *@Data:19:04 2018-01-04
    */
    public String update(){

        return "update";
    }

    /**
    *@Description:成功更新信息
    *@Data:20:08 2018-01-04
    */
    public String updateSucc(){
        int result = userService.updateUserInfo(user);
        if (result>0) {
            ServletActionContext.getRequest().setAttribute("result",result);
            return "updateSucc";
        }
        return "faile";
    }

    public String tzUpdatePwd(){
        return "tzUpdatePwd";
    }

    /**
    *@Description:更新密码
    *@Data:9:39 2018-01-05
    */
    public String updatePwd(){
        int result = userService.updatePwdUserInfo(user);
        if (result>0) {
            ServletActionContext.getRequest().setAttribute("result",result);
            return "updatePwd";
        }
        return "faile";
    }
    /**
    *@Description:员工管理
    *@Data:20:43 2018-01-04
    */
    public String manage(){
        Page page = userService.manageUser(user.getDeptId(),info,CurrentPage);
        ValueStack vs = ActionContext.getContext().getValueStack();
        vs.set("page",page);
        return "manage";
    }

    /**
    *@Description:更新员工信息
    *@Data:13:38 2018-01-05
    */
    public String updateEmployee(){
        User u = userService.updateEmployeeUserInfo(user);
        ValueStack vs = ActionContext.getContext().getValueStack();
        vs.set("user",u);
        return "updateEmployee";
    }
    /**
    *@Description:成功更新员工信息
    *@Data:15:55 2018-01-05
    */
    public String updateEmployeeSucc(){
        int result = userService.updateEmployeeSucc(user);
        return "updateEmployeeSucc";
    }

    /**
    *@Description:跳转到增加员工
    *@Data:9:32 2018-01-06
    */
    public String add(){
        return "add";
    }
    /**
    *@Description:添加用户成功
    *@Data:13:20 2018-01-06
    */
    public String addSucc(){
        int result = userService.addUser(user);
        return "addSucc";
    }
/**
*@Description:ajax验证
*@Data:19:47 2018-01-04
*/
    public void checkUsername() throws IOException {
        User user1= userService.checkLogin(name);
        HttpServletResponse response = ServletActionContext.getResponse();
        if (user1==null){
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print("true");
        }
    }

    /**
    *@Description:ajax请求查找所有的部门
    *@Data:14:56 2018-01-05
    */
    public void selectDept() throws IOException {
        List<Department> departmentList = userService.selectDept();
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().print(JSON.toJSONString(departmentList));
    }
    @Override
    public User getModel() {
        return user;
    }
}
