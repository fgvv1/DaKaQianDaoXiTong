package com.krt.user.entity;

import com.krt.dept.entity.Department;

import java.util.Date;
import java.util.List;

public class User {
    private Integer userId;//用户id

    private String username;//用户名

    private String password;//用户密码

    private String realName;//用户角色名

    private Byte sex;//用户的性别

    private String mobile;//用户电话

    private Integer deptId;//用户部门号id

    private Integer roleId;//用户角色名id

    private String introduction;//用户自我介绍

    private Date creatTime;//用户入职时间

    private Integer userStatus;//用户是否离职状态

    private List<SMenu> sMenuList;//第二菜单，用户shrio认证权限

    private Department department;//部门对象，多表连接

    private Role role;//角色对象，多表连接

    private int logSize;//用户所写的日志数量

    public int getLogSize() {
        return logSize;
    }

    public void setLogSize(int logSize) {
        this.logSize = logSize;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<SMenu> getsMenuList() {
        return sMenuList;
    }

    public void setsMenuList(List<SMenu> sMenuList) {
        this.sMenuList = sMenuList;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }
}