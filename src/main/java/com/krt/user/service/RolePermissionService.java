package com.krt.user.service;

import com.krt.user.entity.RolePermission;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
public interface RolePermissionService {
    int addPermission(RolePermission rolePermission);

    int deleteRP(RolePermission rolePermission);
}
