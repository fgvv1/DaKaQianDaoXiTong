
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>用户添加</title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/table.css"/>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath }/static/skin/css/form.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath }/static/skin/js/form.js"></script>
    <script>
        $(function () {
            $("#roleId").val("${user.roleId}");
            $("#sex").val("${user.sex}");
            $("#userStatus").val("${user.userStatus}");
            var url = "${pageContext.request.contextPath}/selectDept_user";
            $.post(url,function (data) {
                $(data).each(function (i,n) {
                    var vsId = "${user.department.deptId}";
                    if(vsId == n.deptId){
                        $("#deptId").append("<option value='"+n.deptId+"' selected>"+n.deptName+"</option>");
                    }else{
                        $("#deptId").append("<option value='"+n.deptId+"'>"+n.deptName+"</option>");
                    }
                });
            },"json")
            $("#checkButton").click(function () {
                var pwd =$("#checkPwd").val();
                if(pwd=="${userInfo.password}"){
                    $("#submit").submit();
                }else {
                    alert("密码错误");
                }
            })
        })
        function checkName() {
            var url = "${pageContext.request.contextPath}/checkUsername_user";
            var username = $("#username").val();
            $.ajax({
                type:"post",
                data:{"name":username},
                async:false,
                url:url,
                success:function (data) {
                    if(data!="true"){
                        alert("账号已经被使用");
                    }
                }
            })
        }
    </script>
</head>
<body>
<div style="border-bottom: solid ;border-width:1px;padding-top: 30px;padding-left: 30px;border-color: #CCCCCC">
    <span style="border: solid;border-color: #CCCCCC; border-width:1px;font-size: 16px;padding: 10px 10px 0px 10px"><b>用户添加</b></span></hr>
</div>
<form action="${pageContext.request.contextPath}/addSucc_user" method="post" id="submit">
    <table class="fm_tab">
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">姓名：</span></td>
            <td class="fm_td2"><input type="text" name="realName" value="" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">用户名：</span></td>
            <td class="fm_td2"><input id="username" onblur="checkName()" name="username" type="text" value="" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">部门：</span></td>
            <td width="35%" class="fm_td2">
                <select name="deptId" id="deptId">
                    <option>请选择</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">性别：</span></td>
            <td width="35%" class="fm_td2">
                <select name="sex" id="sex">
                    <option value="0">男</option>
                    <option value="1">女</option>
                </select>
            </td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">职位：</span></td>
            <td width="35%" class="fm_td2">
                <select name="roleId" id="roleId">
                    <option value="0">普通员工</option>
                    <option value="1">部长</option>
                    <c:if test="${userInfo.roleId==3}">
                        <option value="2">总经理</option>
                    </c:if>
                </select>
            </td>
        </tr>
        <tr>
            <td class="fm_td1 fyh"><span class="fm_text">手机号码：</span></td>
            <td class="fm_td2"><input id="tl" name="mobile" type="text" value="" class="fm_tt1" style="width:25%"/></td>
        </tr>
        <tr>
            <td width="15%" class="fm_td1 fyh"><span class="fm_text">个人介绍：</span></td>
            <td width="35%" class="fm_td2"><input type="text" name="introduction" value=""  placeholder="这个人很懒，什么也没有留下"    class="fm_tt1" style="width:25%"/></td>
        </tr>
    </table>
    <div style="padding-top: 20px;padding-left: 300px;">
        <input style="width: 120px;"  type="button" id="submit_id" class="fm_button fyh submit_id" value="添&nbsp;&nbsp;加"/>
    </div>
</form>

<div class="form_zz">
    <div class="form_zzbg"></div>
    <div class="form_zzcontent">
        <h1 class="fyh form_zztit"><img src="/static/skin/images/form_zz_icon_03.png" class="form_zzimg"/>提示信息</h1>
        <div class="form_ts fyh">
            请输入密码:<input id="checkPwd" type="text" placeholder="" >
        </div>
        <div class="form_zzbutton">
            <input id="checkButton" type="button" value="确&nbsp;定" class="form_zzan1 fyh"/>
            <input type="button" value="取&nbsp;消" class="form_zzan2 fyh"/>
        </div>
    </div>
</div>


</body>
</html>
