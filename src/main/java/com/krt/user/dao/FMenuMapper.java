package com.krt.user.dao;

import com.krt.user.entity.FMenu;

public interface FMenuMapper {
    int deleteByPrimaryKey(Integer fMenuId);

    int insert(FMenu record);

    int insertSelective(FMenu record);

    FMenu selectByPrimaryKey(Integer fMenuId);

    int updateByPrimaryKeySelective(FMenu record);

    int updateByPrimaryKey(FMenu record);
}