package com.krt.user.service.impl;

import com.krt.common.Page;
import com.krt.dept.dao.DepartmentMapper;
import com.krt.dept.entity.Department;
import com.krt.user.dao.UserMapper;
import com.krt.user.entity.User;
import com.krt.user.service.UserService;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.security.auth.Subject;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017-12-22.
 */
@Transactional
@Service(value = "userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DepartmentMapper departmentMapper;
    /**
    *@Description:校验登录功能
    *@Data:21:54 2017-12-22
    */
    @Override
    public User checkLogin(String username) {
        User user= userMapper.checkLogin(username);
        return user;
    }
/**
*@Description:得到该用户的权限
*@Data:18:41 2017-12-25
*/
    @Override
    public User findByUser(String username) {
        User user= userMapper.findByUser(username);
        return user;
    }

    @Override
    public int updateUserInfo(User user) {
        User user1 = (User) ServletActionContext.getRequest().getSession().getAttribute("userInfo");
        user1.setUsername(user.getUsername());
        user1.setMobile(user.getMobile());
        user1.setIntroduction(user.getIntroduction());
        return userMapper.updateByPrimaryKeySelective(user1);
    }

    @Override
    public int updatePwdUserInfo(User user) {
        User user1 = (User) ServletActionContext.getRequest().getSession().getAttribute("userInfo");
        user1.setPassword(user.getPassword());
        return userMapper.updateByPrimaryKeySelective(user1);
    }
    /**
    *@Description:更新员工的信息
    *@Data:14:35 2018-01-05
    */
    @Override
    public User updateEmployeeUserInfo(User user) {
        User u= userMapper.selectByPrimaryKey(user.getUserId());
        User user1=userMapper.checkLogin(u.getUsername());
        return user1;
    }

    @Override
    public List<Department> selectDept() {
        return departmentMapper.selectDept();
    }

    @Override
    public int updateEmployeeSucc(User user) {
        User user1 = userMapper.selectByPrimaryKey(user.getUserId());
        user.setCreatTime(user1.getCreatTime());
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public int addUser(User user) {
        user.setPassword("123456");
        user.setCreatTime(new Date());
        return userMapper.insertSelective(user);
    }

    @Override
    public List<User> selectUserByNoDept() {
        return userMapper.selectUserByNoDept();
    }

    @Override
    public List<User> getUserByDept(Integer deptId) {
        return userMapper.selectUserByDept(deptId,null,0,5);
    }

    @Override
    public List<User> getUser() {
        return userMapper.selectUser(null,null,0,5);
    }

    /**
    *@Description:管理员工操作
    *@Data:21:02 2018-01-04
    */
    @Override
    public Page manageUser(Integer deptId,String info,Integer CurrentPage) {
        Page page = new Page();
        //职位不同，所管理的员工也不一样
        User user = (User) ServletActionContext.getRequest().getSession().getAttribute("userInfo");
        if (user.getRoleId()==1){
            //得到底下员工的总数
            page.setTotalRows(userMapper.getTotalUserSizeByDept(user.getDeptId(),info));
            CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
            CurrentPage=(CurrentPage<=0)?1:CurrentPage;
            page.setCurrentPage(CurrentPage);
            //分页查询
            page.setItems(userMapper.selectUserByDept(user.getDeptId(),info,page.getStartRow(),page.getPageSize()));
            return page;
        }
        if (user.getRoleId()==2||user.getRoleId()==3){
            //得到底下员工的总数
            page.setTotalRows(userMapper.getTotalUserSize(deptId,info));
            CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
            CurrentPage=(CurrentPage<=0)?1:CurrentPage;
            page.setCurrentPage(CurrentPage);
            //分页查询
            page.setItems(userMapper.selectUser(deptId,info,page.getStartRow(),page.getPageSize()));
            return page;
        }
        return null;
    }


}
