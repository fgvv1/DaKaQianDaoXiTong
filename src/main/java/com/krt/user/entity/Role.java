package com.krt.user.entity;

import java.util.List;

public class Role {
    private Integer roleId;//角色id

    private String roleName;//角色名称

    private List<SMenu> sMenuList;//二菜单集合封装

    public List<SMenu> getsMenuList() {
        return sMenuList;
    }

    public void setsMenuList(List<SMenu> sMenuList) {
        this.sMenuList = sMenuList;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }
}