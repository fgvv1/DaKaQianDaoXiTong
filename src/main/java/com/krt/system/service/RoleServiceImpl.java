package com.krt.system.service;

import com.krt.common.Page;
import com.krt.user.dao.RoleMapper;
import com.krt.user.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-07.
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {
@Autowired
private RoleMapper roleMapper;
    /**
    *@Description:查询到所有的权限，进行管理
    *@Data:17:27 2018-01-08
    */
    @Override
    public Page getAllRolePermission(Integer CurrentPage) {
        Page page=new Page();
        page.setTotalRows(roleMapper.getAllRolePermissionSize());
        CurrentPage=((CurrentPage<page.getTotalPage()) ? CurrentPage : page.getTotalPage());
        CurrentPage=(CurrentPage<=0)?1:CurrentPage;
        page.setCurrentPage(CurrentPage);
        page.setItems(roleMapper.getAllRolePermission(page.getStartRow(),page.getPageSize()));
        return page;
    }
}
