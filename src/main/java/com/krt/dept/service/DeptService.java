package com.krt.dept.service;

import com.krt.dept.entity.Department;

import java.util.List;

/**
 * Created by 陈汉帅 on 2018-01-06.
 */
public interface DeptService {
    List<Department> getAllDept();

    void submitDeptDescripte(String deptDescripte, int id);

    int deleteDept(Department department);

    int insertDept(Department department, List<String> employee);
}
